Lead Yourself First: Inspiring Leadership Through Solitude
==========================================================================================

Author: Raymond M. Kethledge and Michael S. Erwin

ISBN: 978-1-63286-631-8

Summary Date: 13 February 2018



The Book in 3 Sentences
-----------------------

Thoroughly processing complex situations requires the ability to step back, isolate
one's self from the clamor of inputs, and sift through the data.  In today's world
this act of self-sequestration is becoming ever-more difficult due to the prevalence
of instant electronic communication and the societal expectation of constant
connection.  This book provides a collection of perspectives from historical and
contemporary leaders that helps motivate the need for solitude and can hopefully
equip one with evidence to use when defending *why* one unplugs.




Summary
-------

The book is divided into several parts, with a collection of accounts which
illustrate the use of solitude to achieve particular goals.

* Clarity

  * Analytical clarity: Dwight Eisenhower, 1944 (D-Day)

  * Intuition: Jane Goodall, 1960 (Entering into ape environment to observe them,
    rather than keeping farther away)


* Creativity

  * T.E. Lawrence, 1917 (Lawrence of Arabia)


* Emotional Balance

  * Acceptance: Abraham Lincoln, 1863 (Lost opportunity to likely end the war
    immediately following the Battle of Gettysburg)

  * Catharsis: Ulysses Grant, 1864

  * Magnanimity: Aung San Suu Kyi, 1990


* Moral Courage

  * Sublime Power to Rise Above, Winston Churchill, 1938 (Publicly opposed
    Chamberlain's appeasement of Hitler, despite massive public support for that
    approach)

  * No Never Alone: Martin Luther King, Jr., 1956 (Entry of MLK into the Civil
    Rights movement)

  * The Dignity Not to Conform: Pope John Paul II, 1979 (Pope John Paul II's journey
    to through the Church and his forgiveness of his attempted assassin)




Final comments
--------------

I see this book as an opportunity to get motivation for carving out solitude in
one's life, as well as providing a body of evidence to assist when explaining to
others why one unplugs.  Like many books which profile leaders, I didn't get an
"ah-ha" moment when reading it, but I was very satisfied with the overall
experience.

The book is an easy read.
